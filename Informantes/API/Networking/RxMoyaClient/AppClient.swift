//
//  AppClient.swift
//  Informantes-ios
//
//  Created on 5/18/17.
//  Copyright © 2017 Kogi. All rights reserved.
//

import Foundation
import Moya
import RxSwift
import KeychainAccess
import SwiftyUserDefaults
import SwiftDate
import Gloss

final class AppClient {
  // MARK: - Singleton
  static let sharedInstance = AppClient()

  // MARK: - Private enums
  fileprivate enum InternalError: Swift.Error {
    case noCredentialsFound
    case unexpectedResponse
  }

  // MARK: - Private constants
  fileprivate static let authTokenKey = "token"
  fileprivate static let credentialsKey = "credentials"
  fileprivate static let loggedInUserKey = "logged_in_user"
  fileprivate static let deviceKey = "device"
  fileprivate static let userUpdateInfoFromLoginKey = "userUpdateInfoFromLogin"
  fileprivate static let currentAvailabilityKey = "availability"

  // MARK: - Private properties
  fileprivate let disposeBag = DisposeBag()
  fileprivate let keychain = Keychain(service: Constants.bundleId)
  //	fileprivate let cache = KingCache()
  fileprivate var authToken: String? {
    didSet {
      print("Auth Token set: \(String(describing: self.authToken))")
      keychain[AppClient.authTokenKey] = authToken
    }
  }

  fileprivate var userUpdateInfoFromLogin: String? {
    didSet {
      keychain[AppClient.userUpdateInfoFromLoginKey] = userUpdateInfoFromLogin
    }
  }

  fileprivate lazy var provider: RxMoyaProvider<MoyaTarget> = { [unowned self] in
    var networkActivityCount = 0
    let provider = RxMoyaProvider<MoyaTarget>(
      endpointClosure: { [unowned self] in
        $0.endpoint(authToken: self.authToken)
      },
      stubClosure: RxMoyaProvider.neverStub,
      plugins: [
        NetworkActivityPlugin {
          switch $0 {
          case .began:
            networkActivityCount += 1
          case .ended:
            networkActivityCount -= 1
          }

          if networkActivityCount == 0 {
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
          } else if networkActivityCount == 1 {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
          }
        }])
    //				, NetworkLoggerPlugin()]) // this will debug the calls to the server
    return provider
    }()

  // MARK: - Internal properties
  fileprivate(set) var device: Device? {
    didSet {
      do {
        try keychain.set(device, key: AppClient.deviceKey)
      } catch {
        print("\(error)")
      }
    }
  }

  // MARK: - Initialization functions
  init(ignoreCachedCredentials: Bool = false) {
    if ignoreCachedCredentials {
      return
    }

    if !Defaults[.recurrentLaunch] {
      setKeychainValuesNil()
      Defaults[.recurrentLaunch] = true
    } else {
      authToken = keychain[AppClient.authTokenKey]
    }
  }
}

// MARK: - NetworkingClient protocol conformance
extension AppClient: NetworkingClient {

  // MARK: - Internal properties

  var accessToken: String? {
    return authToken
  }

  var isUserLoggedIn: Bool {
    return (authToken != nil)
  }

  var isUserInfoUpdate: Bool {
    return (userUpdateInfoFromLogin != nil)
  }

  // MARK: - Internal methods
  func logUserOut() {
    setKeychainValuesNil()
    NotificationCenter.default
      .post(name: Notifications.appClientDidSignOut,
            object: self,
            userInfo: nil)
  }

  func setKeychainValuesNil() {
    authToken = nil
    device = nil
    userUpdateInfoFromLogin = nil
  }

  // Auth API Calls
  func registerDevice(device: Device) -> Observable<Void> {
    self.device = device
    return provider.request(.registerDevice(device: device))
      .filterApiErrors()
      .map { (_) in return () }
  }

  func unregisterDevice(device: Device) -> Observable<Void> {
    self.device = device
    return provider.request(.unregisterDevice(device: device))
      .filterApiErrors()
      .map { (_) in return () }
  }
}

private extension ObservableType where E == Response {
  func updateAuthTokenIfAvailable(client: AppClient) -> Observable<Response> {
    return `do`(onNext: { [weak client] (response) in
      if let httpResponse = response.response as? HTTPURLResponse,
        let authToken = httpResponse.allHeaderFields["Authorization"] as? String {
        client?.authToken = authToken
      } else {
        do {
          // swiftlint:disable:next force_cast
          let parsedData = try JSONSerialization.jsonObject(with: response.data, options: []) as! [String: Any]
          if let authToken = parsedData["token"] as? String {
            client?.authToken = authToken
          }
        }
      }
    })
  }

  func filterApiErrors() -> Observable<Response> {
    return `do`(onNext: { (response) in
      do {
        _ = try response.filterSuccessfulStatusCodes()
      } catch {
        if let apiError: ApiError = try? response.mapObject() {
          throw apiError
        }
        throw error
      }
    })
  }
}

extension Keychain {
  func getDecoded<T: Decodable>(_ key: String) throws -> T? {
    if let data = try getData(key),
      let json = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions()) as? [String: AnyObject],
      let value = T(json: json) {
      return value
    }
    return nil
  }

  func set<T: Encodable>(_ value: T?, key: String) throws {
    guard let json = value?.toJSON(),
      let data = try? JSONSerialization.data(withJSONObject: json, options: JSONSerialization.WritingOptions()) else {
        self[data: key] = nil
        return
    }
    try set(data, key: key)
  }
}
