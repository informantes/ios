//
//  MoyaTarget.swift
//  Informantes-ios
//
//  Created on 5/18/17.
//  Copyright © 2017 Kogi. All rights reserved.
//

import Foundation
import UIKit
import Moya
import Alamofire
import SwiftyUserDefaults
import Localize_Swift

enum MoyaTarget {

  //Auth
  case login(username: String, password: String, division: String)
  case myProfile()

  //User
  case registerDevice(device: Device)
  case unregisterDevice(device: Device)
//  case updateUserInformation(user: User)
  case logout()

  fileprivate static let fetchCount = 10
}

extension MoyaTarget: TargetType {
  var baseURL: URL {
    return Defaults[.apiEnvironment].apiBaseUrl
  }
  var path: String {
    switch self {
    case .login(_):
      return "sessions"
//    case .myProfile(), .updateUserInformation(_):
//      return "users/me"
    case .registerDevice(let device), .unregisterDevice(let device):
      print("users/me/devices/\(String(describing: device.token))")
      return "users/me/devices/\(String(describing: device.token))"

    default:
      return ""
    }
  }
  var method: Moya.Method {
    switch self {
    case .login(_):
      return .post

    case .registerDevice(_):
      return .put

//    case .updateUserInformation(_):
//      return .patch

    case .unregisterDevice(_):
      return .delete

    default:
      return .get
    }
  }
  var parameters: [String: Any]? {
    switch self {

//    case .updateUserInformation(let user):
//      guard let firstname = user.firstname, let lastname = user.lastname, let email = user.email, let cellNumber = user.cellNumber else {
//        return [:]
//      }
//      return ["firstName": firstname, "lastName": lastname, "emailAddress": email, "cellNumber": cellNumber, "isAttendee": true]

    case .login(let username, let password, let division):
      return ["username": username, "password": password, "division": division]

    default:
      return nil
    }
  }
  var sampleData: Data {
    return SampleDataHelper.sampleFor(target: self)
  }
  var task: Task {
    switch self {
//    case .uploadSignature(_, image: let image):
//      guard let jpgData = UIImageJPEGRepresentation(image, 1.0) else { return .request }
//      return .upload(UploadType.multipart([MultipartFormData(provider: .data(jpgData), name: "signature", fileName: "test.jpg", mimeType: "image/jpeg")]))
    default:
      return .request
    }
  }
  var shouldPassToken: Bool {
    switch self {
    case .login(_):
      return false
    default:
      return true
    }
  }
  var validate: Bool {
    return false
  }
  var acceptLanguage: String? {
    return Localize.currentLanguage()
  }
  var parameterEncoding: ParameterEncoding {
    switch self {
//    case .updateUserInformation(_):
//      return JSONEncoding.default
//    case .:
//      return CompositeEncoding()
    default:
      return URLEncoding.default
    }
  }

  // MARK: - Internal functions
  func endpoint(authToken: String?) -> Endpoint<MoyaTarget> {
    var endpoint: Endpoint<MoyaTarget>
    endpoint = MoyaProvider.defaultEndpointMapping(for: self)

    if let authToken = authToken, shouldPassToken {
      endpoint = endpoint.adding(newHTTPHeaderFields: ["Authorization": authToken])
    }

    return endpoint
  }

  private struct CompositeEncoding: ParameterEncoding {
    public func encode(_ urlRequest: URLRequestConvertible, with parameters: Parameters?) throws -> URLRequest {
      guard let parameters = parameters else {
        return try urlRequest.asURLRequest()
      }

      let queryParameters = (parameters["query"] as? Parameters)
      let queryRequest = try URLEncoding(destination: .queryString).encode(urlRequest, with: queryParameters)

      if let body = parameters["body"] {
        let bodyParameters = (body as? Parameters)
        var bodyRequest = try JSONEncoding().encode(urlRequest, with: bodyParameters)

        bodyRequest.url = queryRequest.url
        return bodyRequest
      } else {
        return queryRequest
      }
    }
  }
}
