//
//  Constants.swift
//  Informantes-ios
//
//  Created on 5/16/17.
//  Copyright © 2017 Kogi. All rights reserved.
//

import Foundation
import UIKit
import SwiftyUserDefaults

struct Constants {
    static let appName = "Informantes"
    static let secondsToDisapearMessages: DispatchTime =  DispatchTime.now() + 2
    static let bundleId = "com.manuelurrego.informantes"
    static let deviceToken = "deviceToken"
    static let pagelimit = 30
    static let frontEndUrl = Defaults[.apiEnvironment].frontEndUrl

    #if BETA
    static let isBeta: Bool = true
    #else
    static let isBeta: Bool = false
    #endif
}

struct Notifications {

    struct AppClientKeys {
        static let id = "id"
        static let user = "user"
    }

    static let appClientDidSignIn = Notification.Name("appClientDidSignIn")
    static let appClientDidSignOut = Notification.Name("appClientDidSignOut")
    static let appClientDidUpdateUser = Notification.Name("appClientDidUpdateUser")
}

enum ApiEnvironment: String {
    case production
    case development

    static let allValues: [ApiEnvironment] = [.production, .development]
}

extension ApiEnvironment {
    var apiBaseUrl: URL {
        switch self {
        case .production:
            return URL(string: "https://production.com/v1/")!
        case .development:
            return URL(string: "https://development.com/v1/")!
        }
    }

    var frontEndUrl: String {
        switch self {
        case .production:
            return "https://google.com/"
        case .development:
            return "http://google.com/"
        }
    }
}
