//
//  NetworkingClient.swift
//  Informantes-ios
//
//  Created on 5/18/17.
//  Copyright © 2017 Kogi. All rights reserved.
//

import Foundation
import RxSwift

protocol NetworkingClient {

  var isUserLoggedIn: Bool { get }
  var isUserInfoUpdate: Bool { get }
  var accessToken: String? { get }
  var device: Device? { get }

  func logUserOut()

  // User
  func registerDevice(device: Device) -> Observable<Void>
  func unregisterDevice(device: Device) -> Observable<Void>
}
