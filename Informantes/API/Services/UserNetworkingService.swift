//
//  UserNetworkingService.swift
//  Informantes-ios
//
//  Created on 5/18/17.
//  Copyright © 2017 Kogi. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

struct UserNetworkingService {

  // MARK: - Private Properties
  fileprivate static let client: NetworkingClient = AppClient.sharedInstance

  // MARK: - static methods
  static func getAccessToken() -> String? {
      return self.client.accessToken
  }

  static func isUserLoggedIn() -> Bool {
    return self.client.isUserLoggedIn
  }

  static func isUserInfoUpdate() -> Bool {
    return self.client.isUserInfoUpdate
  }

  static func currentDevice() -> Device? {
    return self.client.device
  }

  static func logout() {
    self.client.logUserOut()
  }

  static func registerDevice(device: Device) -> Observable<Void> {
    return self.client.registerDevice(device: device)
  }

  static func unregisterDevice(device: Device) -> Observable<Void> {
    return self.client.unregisterDevice(device: device)
  }
}
