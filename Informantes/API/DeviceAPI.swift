//
//  DeviceAPI.swift
//  Informantes-ios
//
//  Created on 5/16/17.
//  Copyright © 2017 Kogi. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

struct DeviceAPI {

  static func getAccessToken() -> String? {
    return UserNetworkingService.getAccessToken()
  }

  static func getCurrentDevice() -> Device? {
    return UserNetworkingService.currentDevice()
  }

  static func registerDevice(device: Device) -> Observable<Void> {
    return UserNetworkingService.registerDevice(device: device)
  }

  static func unregisterDevice(device: Device) -> Observable<Void> {
    return UserNetworkingService.unregisterDevice(device: device)
  }
}
