//
//  AppCoordinator.swift
//  Informantes-ios
//
//  Created on 5/16/17.
//  Copyright © 2017 Kogi. All rights reserved.
//

import UIKit

class AppCoordinator: Coordinator {

    // MARK: - Private Properties
    fileprivate var window: UIWindow
    fileprivate var coordinators: CoordinatorsDictionary

    // MARK: - Properties
    var rootViewController: UIViewController {
        let coordinator = coordinators.popFirst()!.1
        return coordinator.rootViewController
    }

    // MARK: - Initializers
    init(window: UIWindow) {
        self.window = window
        coordinators = [:]
    }

    // MARK: - Coordinator
    func start() {
        showHome()
    }

    // MARK: Helpers
    fileprivate func showHome() {
        let homeCoordinator = HomeCoordinator()
        homeCoordinator.delegate = self
        coordinators[HomeCoordinator.name] = homeCoordinator
        window.rootViewController = homeCoordinator.rootViewController
        homeCoordinator.start()
    }
}

extension AppCoordinator: HomeCoordinatorDelegate {
    func userDidLogoutFromApp() {
        //showLogin()
    }
}
