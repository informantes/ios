//
//  ManageMyQueueCoordinator.swift
//  cca-convention-swift
//
//  Created by Alex Arboleda on 5/19/17.
//  Copyright © 2017 Kogi. All rights reserved.
//

import UIKit

class ManageMyQueueCoordinator: Coordinator {
  // MARK: - Properties
  var rootViewController: UIViewController
  fileprivate var coordinators: [String: Coordinator]
  fileprivate var navigationController: UINavigationController {
    if let navigationController = rootViewController  as? UINavigationController {
      return navigationController
    }
    return UINavigationController()
  }

  // MARK: - Initializers
  init() {
    rootViewController = UINavigationController()
    coordinators = [:]
  }

  // MARK: - Coordinator
  func start() {
    showManageMyQueueScreen()
  }

  // MARK: - Helpers
  fileprivate func showManageMyQueueScreen() {
    let manageMyQueueVM = ManageMyQueueViewModel()
    let manageMyQueueVC = ManageMyQueueViewController(viewModel: manageMyQueueVM)
    manageMyQueueVM.coordinatorDelegate = self

    navigationController.setViewControllers([manageMyQueueVC], animated: true)
  }
}

extension ManageMyQueueCoordinator: ManageMyQueueViewModelCoordinatorDelegate {}
