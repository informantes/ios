//
//  HomeCoordinator.swift
//  Informantes-ios
//
//  Created on 5/16/17.
//  Copyright © 2017 Kogi. All rights reserved.
//

import UIKit
import Foundation
import RxSwift

protocol HomeCoordinatorDelegate: class {
    func userDidLogoutFromApp()
}

class HomeCoordinator: Coordinator {

    // MARK: - Properties
    var rootViewController: UIViewController
    fileprivate var coordinators: CoordinatorsDictionary

    fileprivate var navigationController: UINavigationController {
        if let navigationController = rootViewController  as? UINavigationController {
            return navigationController
        }
        return UINavigationController()
    }

    weak var delegate: HomeCoordinatorDelegate?
    fileprivate let disposeBag = DisposeBag()

    // MARK: - Initializers
    init() {
        rootViewController = UINavigationController()
        coordinators = [:]
        //    updateDeviceToken()
    }

    // MARK: - Coordinator
    func start() {
        showHomeScreen()
    }

    func showHomeScreen() {
        let homeVM = HomeViewModel()
        homeVM.coordinatorDelegate = self
        let homeVC = HomeViewController(viewModel: homeVM)
        navigationController.setViewControllers([homeVC], animated: true)
    }

    func updateDeviceToken() {
        // swiftlint:disable:next force_cast
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.updateDeviceToken()
    }

}

// MARK: - HomeViewModelCoordinatorDelegate
extension HomeCoordinator: HomeViewModelCoordinatorDelegate {

    func homeViewModelShowMenu(_ viewModel: HomeViewModel) {
        //homeContainerViewController.presentMenuViewController()
    }
}
