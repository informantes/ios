//
//  MyInfoCoordinator.swift
//  Informantes-ios
//
//  Created on 6/29/17.
//  Copyright © 2017 Kogi. All rights reserved.
//

import Foundation

protocol MyInfoCoordinatorDelegate: class {
  func userDidUpdateInformation()
}

class MyInfoCoordinator: Coordinator {
  // MARK: - Properties
  var rootViewController: UIViewController
  fileprivate var coordinators: [String: Coordinator]
  fileprivate var navigationController: UINavigationController {
    if let navigationController = rootViewController  as? UINavigationController {
      return navigationController
    }
    return UINavigationController()
  }

  weak var delegate: MyInfoCoordinatorDelegate?

  // MARK: - Initializers
  init() {
    rootViewController = UINavigationController()
    coordinators = [:]
  }

  // MARK: - Coordinator
  func start() {
    showMyInfoScreen()
  }

  // MARK: - Helpers
  fileprivate func showMyInfoScreen() {
    let myInfoVM = MyInfoViewModel()
    let myInfoVC = MyInfoViewController(viewModel: myInfoVM)
    myInfoVM.coordinatorDelegate = self

    navigationController.setViewControllers([myInfoVC], animated: true)
  }

  fileprivate func dismiss() {
    delegate?.userDidUpdateInformation()
  }
}

extension MyInfoCoordinator: MyInfoViewModelCoordinatorDelegate {
  func myInfoViewModelSuccessUpdate(_ viewModel: MyInfoViewModel) {
    self.dismiss()
  }
}
