//
//  Coordinator.swift
//  Informantes-ios
//
//  Created on 5/16/17.
//  Copyright © 2017 Kogi. All rights reserved.
//

import UIKit

typealias CoordinatorsDictionary = [String: Coordinator]

protocol Coordinator {
  var rootViewController: UIViewController { get }

  func start()
}

extension Coordinator {
  static var name: String {
    return String(describing: self)
  }
}
