//
//  ApiError.swift
//  Informantes-ios
//
//  Created on 5/16/17.
//  Copyright © 2017 Kogi. All rights reserved.
//

import Foundation
import Gloss

struct ApiError: Error {
  let code: Int
  let description: String?
  let error: String?
  let body: String?
}

enum ApiErrorCode: Int {
  // Internal error codes
  case unknown           = -100
  case couldNotSignToken = -102
  case castingError      = -103

  case streamSortByDateCannotbeBlank = 102

  case invalidEmailDomain = 201
  case invalidCode = 202

  case tokenAlreadyRegistered = 409
  case badRequest = 400

}

extension ApiError: Decodable {
  fileprivate struct JsonKeys {
    static let code = "statusCode"
    static let description = "message"
    static let error = "error"
    static let body = "body"
  }

  init?(json: JSON) {
    guard let code: Int = JsonKeys.code <~~ json
      else { return nil }
    self.code = code
    self.description = JsonKeys.description <~~ json
    self.error = JsonKeys.error <~~ json
    self.body = JsonKeys.error <~~ json

    if let bodyMessage = self.body {
      print(bodyMessage)
    }
  }
}
