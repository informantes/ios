//
//  Array+Operations.swift
//  Informantes-ios
//
//  Created on 5/23/17.
//  Copyright © 2017 Kogi. All rights reserved.
//

import Foundation

extension Array where Element: Equatable {

  mutating func remove(object: Element) {
    if let index = index(of: object) {
      remove(at: index)
    }
  }

  mutating func remove(objects: [Element]) {
    for item in objects {
        self.remove(object: item)
    }
  }

  func appendToNewArray(newElement: Element) -> Array {
    var result = self
    result.append(newElement)
    return result
  }
}
