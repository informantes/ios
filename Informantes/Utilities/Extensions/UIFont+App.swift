//
//  UIFont+App.swift
//  Informantes-ios
//
//  Created on 5/17/17.
//  Copyright © 2017 Kogi. All rights reserved.
//

import UIKit

enum Font {
  case sfMedium
  case sfRegular
  case sfSemiBold

  enum Size: CGFloat {
    case xsmall = 11
    case small = 13
    case medium = 15
    case large = 17
    case xlarge = 19

    var value: CGFloat {
      return self.rawValue
    }
  }

  func uiFont(size: Font.Size) -> UIFont {
    let name: String
    switch self {
    case .sfMedium:
      name = "SanFranciscoText-Medium"
    case .sfRegular:
      name = "SanFranciscoText-Regular"
    case .sfSemiBold:
      name = "SanFranciscoText-Semibold"
    }
    return UIFont(name: name, size: size.value)!
  }
}

extension UIFont {
  static func listFonts() {
    let fontFamilyNames = UIFont.familyNames
    for familyName in fontFamilyNames {
      print("------------------------------")
      print("Font Family Name = [\(familyName)]")
      let names = UIFont.fontNames(forFamilyName: familyName)
      print("Font Names = [\(names)]")
    }
  }

  func monospaced() -> UIFont {
    let fontFeatures = [
      [UIFontFeatureTypeIdentifierKey: kNumberSpacingType,
       UIFontFeatureSelectorIdentifierKey: kMonospacedNumbersSelector]
    ]
    let descriptorWithFeatures = fontDescriptor.addingAttributes([UIFontDescriptorFeatureSettingsAttribute: fontFeatures])
    return UIFont(descriptor: descriptorWithFeatures, size: 0)
  }
}
