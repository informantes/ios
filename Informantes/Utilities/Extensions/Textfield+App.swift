//
//  Textfield+App.swift
//  Informantes-ios
//
//  Created on 5/24/17.
//  Copyright © 2017 Kogi. All rights reserved.
//

import Foundation
import UIKit

extension UITextField {
  override open func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
    if action == #selector(copy(_:)) || action == #selector(paste(_:)) {
      return false
    }

    return true
  }
}
