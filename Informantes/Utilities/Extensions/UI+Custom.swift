//
//  UI+Custom.swift
//  Informantes-ios
//
//  Created on 5/17/17.
//  Copyright © 2017 Kogi. All rights reserved.
//

import UIKit
import TextAttributes

protocol Customizable {}

// MARK: - UIButton
extension Customizable where Self: UIButton {

  internal func customize(corner: CGFloat = 0) {
    self.layer.cornerRadius = corner
    self.setTitleColor(UIColor.appWhiteColor(), for: .normal)
    self.titleLabel?.font = Font.sfMedium.uiFont(size: Font.Size.medium)
    self.titleLabel?.lineBreakMode = .byTruncatingTail
    self.titleLabel?.numberOfLines = 2
    self.titleLabel?.textAlignment = .center
  }

  func customizeDefault(corner: CGFloat = 4) {
    self.customize(corner: corner)
    self.backgroundColor = UIColor.appComponentColor()
  }

  func customizeGray(corner: CGFloat = 4) {
    self.customize(corner: corner)
    self.backgroundColor = UIColor.appComponentGrayColor()
  }

  func customizeSimple() {
    self.setTitleColor(UIColor.appMainStyleColor(), for: .normal)
    self.titleLabel?.font = Font.sfMedium.uiFont(size: Font.Size.medium)
  }
}

// MARK: - UISearchBar
extension Customizable where Self: UISearchBar {
  func customize() {
    self.barTintColor = UIColor.appComponentGrayColor()
    self.tintColor = UIColor.white
    self.showsCancelButton = true
    for subView in self.subviews {
      for subViewOne in subView.subviews {

        if let textField = subViewOne as? UITextField {
          textField.textColor = UIColor.appFontDefaultColor()
          textField.placeholder = "Search".localized()
        }
      }
    }
  }
}

// MARK: - UILabel
extension Customizable where Self: UILabel {
  func customize() {
    self.font = Font.sfMedium.uiFont(size: Font.Size.large)
  }

  func customizeDefault() {
    self.customize()
    self.textColor = UIColor.appFontDefaultColor()
  }

  func customizeWarning() {
    self.customize()
    self.textColor = UIColor.appFontWarningColor()

  }
}

// MARK: - UIViewController
extension Customizable where Self: UIViewController {
  func customizeBackButton() {
      navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
  }
}

// MARK: - UITableView
extension Customizable where Self: UITableView {
  func customize() {
    self.backgroundColor = UIColor.appLightTableColor()
    self.contentInset = UIEdgeInsets(top: 16, left: 0, bottom: 0, right: 0)
    self.tableFooterView = UIView(frame: .zero)
  }
}

// MARK: Conform protocol
extension UIButton: Customizable {}
extension UILabel: Customizable {}
extension UISearchBar: Customizable {}
extension UIViewController: Customizable {}
extension UITableView: Customizable {}
