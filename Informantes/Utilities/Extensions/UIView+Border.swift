//
//  UIView+Border.swift
//  Informantes-ios
//
//  Created on 6/8/17.
//  Copyright © 2017 Kogi. All rights reserved.
//

import UIKit

extension UIView {

  func addBorder(corner: CGFloat = 0) {
    self.layer.borderColor = UIColor.appMainStyleColor().cgColor
    self.layer.borderWidth = 1
    self.layer.masksToBounds = true
    self.layer.cornerRadius = corner
  }

  func addBorderWith(color: UIColor, thickness: CGFloat, corner: CGFloat) {
    self.layer.borderColor = color.cgColor
    self.layer.borderWidth = thickness
    self.layer.cornerRadius = corner
  }

  func addBorder(color: UIColor, thickness: CGFloat) {
    self.layer.borderColor = color.cgColor
    self.layer.borderWidth = thickness
  }

  func addBorder(edge: UIRectEdge, color: UIColor, thickness: CGFloat) {

    let border = CALayer()

    switch edge {
    case UIRectEdge.top:
      border.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: thickness)
      break
    case UIRectEdge.bottom:
      border.frame = CGRect(x: 0, y: self.frame.height - thickness, width: self.frame.width, height: thickness)
      break
    case UIRectEdge.left:

      border.frame = CGRect(x: 0, y: 0, width: thickness, height: self.frame.height)
      break
    case UIRectEdge.right:

      border.frame = CGRect(x: self.frame.width - thickness, y: 0, width: thickness, height: self.frame.height)
      break
    default:
      break
    }

    border.backgroundColor = color.cgColor

    self.layer.addSublayer(border)
  }

  func addBorder(edge: UIRectEdge) {
    self.addBorder(edge: edge, color: UIColor.appMainStyleColor(), thickness: 1.0)
  }
}
