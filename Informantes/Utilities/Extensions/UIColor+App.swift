//
//  UIColor+App.swift
//  Informantes-ios
//
//  Created on 2/20/17.
//  Copyright © 2017 Kogi Mobile. All rights reserved.
//

import UIKit

extension UIColor {
  convenience init(intRed: Int, intGreen: Int, intBlue: Int, alpha: Float = 1.0) {
    self.init(red: CGFloat(intRed) / 255.0, green: CGFloat(intGreen) / 255.0, blue: CGFloat(intBlue) / 255.0, alpha: CGFloat(alpha))
  }

  convenience init(intRGB: Int, alpha: Float = 1.0) {
    self.init(red: CGFloat(intRGB) / 255.0, green: CGFloat(intRGB) / 255.0, blue: CGFloat(intRGB) / 255.0, alpha: CGFloat(alpha))
  }

  static func appMainStyleColor() -> UIColor {
//    return UIColor(intRed: 55, intGreen: 158, intBlue: 177)
    return UIColor(intRed: 253, intGreen: 190, intBlue: 17)
  }

  static func appNavBarColor() -> UIColor {
    return UIColor.black
  }

  static func appWhiteColor() -> UIColor {
    return UIColor(intRGB: 255)
  }

  static func appComponentColor() -> UIColor {
    return UIColor(intRed: 255, intGreen: 193, intBlue: 0)
  }

  static func appComponentGrayColor() -> UIColor {
    return UIColor(intRGB: 155)
  }

  /**
   Default fonts color
   */
  static func appFontDefaultColor() -> UIColor {
    return UIColor(intRGB: 64)
  }

  static func appFontWarningColor() -> UIColor {
    return UIColor(intRed: 208, intGreen: 2, intBlue: 27)
  }

  static func appFontGreenColor() -> UIColor {
    return UIColor(intRed: 97, intGreen: 154, intBlue: 53)
  }

  static func appFontRedColor() -> UIColor {
    return UIColor(intRed: 255, intGreen: 0, intBlue: 0)
  }

  //  static func appDarkBlueGradientColor() -> UIColor {
  //    return UIColor(intRed: 0, intGreen: 168, intBlue: 203)
  //  }
  //
  //  static func applightBlueGradientColor() -> UIColor {
  //    return UIColor(intRed: 32, intGreen: 202, intBlue: 229)
  //  }
  //

  /**
   User input copy, table/list headers
   */
//  static func appPrimaryTextColor() -> UIColor {
//    return UIColor(intRed: 255, intGreen: 255, intBlue: 255)
//  }

  /**
   Body copy, placeholder text
   */
//  static func appSecondaryTextColor() -> UIColor {
//    return UIColor(intRed: 155, intGreen: 155, intBlue: 155)
//  }

  /**
   Header spacers for tables
   */
  static func appLightGreyColor() -> UIColor {
    return UIColor(intRed: 117, intGreen: 117, intBlue: 117)
  }

  static func appLightTableColor() -> UIColor {
    return UIColor(intRed: 237, intGreen: 237, intBlue: 237)
  }
//
//  static func appModalBackgroundColor() -> UIColor {
//    return UIColor(intRed: 23, intGreen: 23, intBlue: 23)
//  }

  /**
   Accent color
   */
//  static func appAccentOrangeColor() -> UIColor {
//    return UIColor(intRed: 255, intGreen: 140, intBlue: 5)
//  }

  // Navigation Bar
  static func appNavigationBarColor() -> UIColor {
    return appNavBarColor()
  }

  static func appNavigationBarTintColor() -> UIColor {
    return white
  }

  static func appStatusBarStyle() -> UIStatusBarStyle {
    return .lightContent
  }
//
//  static func appPageControlIndicatorWhiteColor() -> UIColor {
//    return UIColor(intRed: 255, intGreen: 255, intBlue: 255, alpha: 0.5)
//  }
}
