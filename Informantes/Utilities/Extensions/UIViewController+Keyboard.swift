//
//  UIViewController+Keyboard.swift
//  Informantes-ios
//
//  Created on 6/23/17.
//  Copyright © 2017 Kogi. All rights reserved.
//

import Foundation

extension UIViewController {
  func hideKeyboard() {
    let tap: UITapGestureRecognizer = UITapGestureRecognizer(
      target: self,
      action: #selector(UIViewController.dismissKeyboard))

    view.addGestureRecognizer(tap)
  }

  func dismissKeyboard() {
    view.endEditing(true)
  }
}
