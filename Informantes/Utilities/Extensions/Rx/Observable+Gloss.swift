//
//  Observable+Gloss.swift
//  Informantes-ios
//
//  Created on 5/19/17.
//  Copyright © 2017 Kogi. All rights reserved.
//

import Foundation
import RxSwift
import Moya
import Gloss

extension Response {
  func mapObject<T: Decodable>() throws -> T {
    guard let json = try mapJSON() as? JSON, let object = T(json: json) else {
      throw Moya.MoyaError.jsonMapping(self)
    }
    return object
  }

  func mapArray<T: Decodable>() throws -> [T] {
    guard let jsonArray = try mapJSON() as? [JSON] else {
      throw Moya.MoyaError.jsonMapping(self)
    }

    return [T].from(jsonArray: jsonArray) ?? []
  }
}
