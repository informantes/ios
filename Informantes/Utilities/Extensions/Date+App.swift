//
//  Date+App.swift
//  Informantes-ios
//
//  Created on 7/6/17.
//  Copyright © 2017 Kogi. All rights reserved.
//

import Foundation

extension Date {

  func dateDisplayFormat() -> String {
    let dateformatter = DateFormatter()
    dateformatter.dateFormat = "dd/MM/yy, h:mm a"

    return dateformatter.string(from: self)
  }
}
