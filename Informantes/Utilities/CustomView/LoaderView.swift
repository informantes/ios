//
//  LoaderView.swift
//  Informantes-ios
//
//  Created on 5/16/17.
//  Copyright © 2017 Kogi. All rights reserved.
//

import Foundation
import SVProgressHUD

struct LoaderView {

  static func setupUI() {
    SVProgressHUD.setDefaultMaskType(.none)
    SVProgressHUD.setForegroundColor(UIColor.appComponentColor())
    SVProgressHUD.setBackgroundColor(UIColor.appWhiteColor())
  }

  static func show() {
    SVProgressHUD.show()
  }

  static func hide() {
    SVProgressHUD.dismiss()
  }
}
