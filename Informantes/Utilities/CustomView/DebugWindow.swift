//
//  DebugWindow.swift
//  Informantes-ios
//
//  Created on 5/16/17.
//  Copyright © 2017 Kogi. All rights reserved.
//

import Foundation
#if BETA
  import UIKit
  import RxSwift
  import SwiftyUserDefaults

  final class DebugWindow: UIWindow {
    // MARK: - Private properties
    private let disposeBag = DisposeBag()
    private var debugMenuEnabled = true
    private var topViewController: UIViewController? {
      var viewController = rootViewController
      while viewController?.presentedViewController != nil {
        viewController = viewController?.presentedViewController
      }
      return viewController
    }

    private var alertStyle: UIAlertControllerStyle {
        return DeviceHelper.isIpad() ? .alert : .actionSheet
    }

    // MARK: - Initialization methods
    init() {
      super.init(frame: .zero)

      NotificationCenter.default
        .rx.notification(NSNotification.Name.UIApplicationWillEnterForeground)
        .subscribeNext { [unowned self] (_) in
          self.debugMenuEnabled = true
        }
        .addDisposableTo(disposeBag)
    }

    required init?(coder aDecoder: NSCoder) {
      fatalError("This init method shouldn't ever be used")
    }

    // MARK: - Motion methods
    override func motionEnded(_ motion: UIEventSubtype, with event: UIEvent?) {
      super.motionEnded(motion, with: event)

      if motion == .motionShake && debugMenuEnabled {
        showDebugMenu()
      }
    }

    // MARK: - Private methods
    private func showDebugMenu() {
      guard let topViewController = topViewController else { return }

      let message: String?
      if let bundleVersion = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as? String,
        let shortVersion = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String {
        message = "\(shortVersion) - \(bundleVersion)"
      } else {
        message = nil
      }

      let alertController = UIAlertController(title: "Debug Menu".localized(), message: message, preferredStyle: alertStyle)
      alertController.addAction(UIAlertAction(title: "Environment: \(Defaults[.apiEnvironment].rawValue.capitalized)", style: .default) { (_) in
        self.showEnvironmentMenu(topViewController: topViewController)
      })
      alertController.addAction(UIAlertAction(title: "Disable Shake Until Next Launch".localized(), style: .default) { [unowned self] (_) in
        self.debugMenuEnabled = false
      })
      alertController.addAction(UIAlertAction(title: "Cancel".localized(), style: .cancel, handler: nil))
      topViewController.present(alertController, animated: true, completion: nil)
    }

    private func showEnvironmentMenu(topViewController: UIViewController) {
      let environmentAlertController = UIAlertController(title: "Choose Environment".localized(), message: nil, preferredStyle: alertStyle)
      for environment in ApiEnvironment.allValues {
        environmentAlertController.addAction(UIAlertAction(title: environment.rawValue.capitalized, style: .default) { [unowned self] (_) in
          self.switchTo(environment: environment, topViewController: topViewController)
        })
      }
      environmentAlertController.addAction(UIAlertAction(title: "Cancel".localized(), style: .cancel, handler: nil))

      topViewController.present(environmentAlertController, animated: true, completion: nil)
    }

    private func switchTo(environment: ApiEnvironment, topViewController: UIViewController) {
      UIAlertController.showFromViewController(topViewController,
                                               title: "Switch Environment".localized(),
                                               message: "Switching environments will force close your app and log your user out. Start it again to start with the new environment.".localized(),
                                               firstButtonTitle: "Cancel".localized(), firstButtonStyle: .cancel,
                                               secondButtonTitle: "Switch".localized(), secondButtonStyle: .default, secondButtonHandler: { _ in
                                                Defaults[.apiEnvironment] = environment
                                                print(Defaults[.apiEnvironment])
                                                exit(0)
      })
    }
  }
#endif
