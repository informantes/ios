//
//  AppearanceHelper.swift
//  Informantes-ios
//
//  Created on 5/17/17.
//  Copyright © 2017 Kogi. All rights reserved.
//

import UIKit
import TextAttributes
import SVProgressHUD

struct AppearanceHelper {
  static func setup() {
    setupStatusBar()
    setupNavigationBar()
    setupRefreshControl()
    setupProgressHUD()
  }

  static fileprivate func setupStatusBar() {
    UIApplication.shared.statusBarStyle = UIColor.appStatusBarStyle()
  }

  static fileprivate func setupNavigationBar() {
    UINavigationBar.appearance().barTintColor = .appNavigationBarColor()
    UINavigationBar.appearance().tintColor = .appNavigationBarTintColor()
    UINavigationBar.appearance().isTranslucent = false
    let navbarTitleAttributes = TextAttributes()
      .font(Font.sfSemiBold.uiFont(size: Font.Size.large))
      .foregroundColor(UIColor.appWhiteColor())
    UINavigationBar.appearance().titleTextAttributes = navbarTitleAttributes.dictionary
  }

  static fileprivate func setupRefreshControl() {
    UIRefreshControl.appearance().tintColor = .appComponentColor()
  }

  static fileprivate func setupProgressHUD() {
    SVProgressHUD.setDefaultStyle(.custom)
    SVProgressHUD.setBackgroundColor(.white)
    SVProgressHUD.setForegroundColor(.appComponentColor())
  }

}
