//
//  DateHelper.swift
//  Informantes-ios
//
//  Created on 7/5/17.
//  Copyright © 2017 Kogi. All rights reserved.
//

import Foundation

struct DateHelper {

  static func getDateFormatter() -> DateFormatter {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ"
    return dateFormatter
  }
}
