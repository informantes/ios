//
//  PaginationHelper.swift
//  Informantes-ios
//
//  Created on 6/12/17.
//  Copyright © 2017 Kogi. All rights reserved.
//

import Foundation

struct PaginationHelper {
  let defaultPageValue = 0
  let initialPage: Int
  var currentPage: Int

  init() {
    initialPage = defaultPageValue
    currentPage = defaultPageValue
  }

  init(initialPage: Int) {
    self.initialPage = initialPage
    currentPage = initialPage
  }

  mutating func increaseCurrentPage() {
    currentPage += 1
  }

  mutating func resetPages() {
    currentPage = initialPage
  }

  func isFirstPage() -> Bool {
    return currentPage == initialPage
  }

  var skip: Int {
    return currentPage * Constants.pagelimit
  }

  mutating func addNewElements<T>(currentElements: inout [T], newElements: [T]) {

    if self.isFirstPage() {
      currentElements = newElements
    } else {
      currentElements.append(contentsOf: newElements)
    }

    increaseCurrentPage()
  }
}
