//
//  EmptyData.swift
//  Informantes-ios
//
//  Created on 6/7/17.
//  Copyright © 2017 Kogi. All rights reserved.
//

import Foundation
import UIKit

struct EmptyData {

  static func defaultMessage() -> NSAttributedString {
    let str = "No data available".localized()
    let attrs = [NSFontAttributeName: UIFont.preferredFont(forTextStyle: UIFontTextStyle.headline)]
    return NSAttributedString(string: str, attributes: attrs)
  }
}
