//
//  DeviceHelper.swift
//  Informantes-ios
//
//  Created on 6/7/17.
//  Copyright © 2017 Kogi. All rights reserved.
//

import Foundation
import UIKit

struct DeviceHelper {

  static func isIpad() -> Bool {
    return UIDevice.current.userInterfaceIdiom == .pad
  }

  static func isIphone() -> Bool {
    return !isIpad()
  }
}
