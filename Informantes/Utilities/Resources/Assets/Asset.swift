//
//  Asset.swift
//  Informantes-ios
//
//  Created on 5/16/17.
//  Copyright © 2017 Kogi. All rights reserved.
//

import UIKit

enum Asset: String {

  //Global

  //Menu
  case menu = "icNavbarMenu"
  case arrowGray = "icDropdownArrowGray"
  case backButton  = "icBackButton"
  case refresh = "icNavbarRefresh"
  case placeholder = "placeholder"

}

extension UIImage {
  convenience init?(asset: Asset) {
    self.init(named: asset.rawValue)
  }
}
