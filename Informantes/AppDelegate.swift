//
//  AppDelegate.swift
//  Informantes-ios
//
//  Created on 5/16/17.
//  Copyright © 2017 Kogi. All rights reserved.
//

import UIKit
import Firebase
import RxSwift
import Fabric
import Crashlytics

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow?
  var appCoordinator: AppCoordinator!
  fileprivate let disposeBag = DisposeBag()

  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {

    Fabric.with([Crashlytics.self])

    // Override point for customization after application launch.
    let urlCache = URLCache(memoryCapacity: 50 * 1024 * 1024, diskCapacity: 200 * 1024 * 1024, diskPath: nil)
    URLCache.shared = urlCache
    AppearanceHelper.setup()
    let window: UIWindow
    #if BETA
      window = DebugWindow()
    #else
      window = UIWindow()
    #endif
    self.window = window

    appCoordinator = AppCoordinator(window: window)
    appCoordinator.start()

    configureNotifications()

    window.frame = UIScreen.main.bounds
    window.makeKeyAndVisible()
    return true
  }

  func applicationWillResignActive(_ application: UIApplication) {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
  }

  func applicationDidEnterBackground(_ application: UIApplication) {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    FIRMessaging.messaging().disconnect()
    print("Disconnected from FCM.")
  }

  func applicationWillEnterForeground(_ application: UIApplication) {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.

  }

  func applicationDidBecomeActive(_ application: UIApplication) {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
  }

  func applicationWillTerminate(_ application: UIApplication) {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
  }
}

extension AppDelegate {

  // MARK: Configure Notifications
  func configureNotifications() {
    let application: UIApplication = UIApplication.shared
    configureNotifications(application)

  }

  func configureNotifications(_ application: UIApplication) {
    let settings: UIUserNotificationSettings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
    application.registerUserNotificationSettings(settings)
    application.registerForRemoteNotifications()

    FIRApp.configure()
  }

  func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
    connectToFcm()

    #if BETA
      //FIRInstanceID.instanceID().setAPNSToken(deviceToken, type: .sandbox)
    #else
      //FIRInstanceID.instanceID().setAPNSToken(deviceToken, type: .prod)
    #endif

    updateDeviceToken()

  }

  func tokenRefreshNotification(_ notification: Notification) {

    //updateDeviceToken()

    connectToFcm()

  }

  func connectToFcm() {
    FIRMessaging.messaging().connect { (error) in
      if error != nil {
        print("Unable to connect with FCM. \(String(describing: error))")
      } else {
        print("Connected to FCM.")
      }
    }
  }

  func updateDeviceToken() {
      if let refreshedToken = FIRInstanceID.instanceID().token() {
        print("InstanceID token: \(refreshedToken)")

        let device: Device = Device(token: refreshedToken)
        DeviceAPI.registerDevice(device: device)
          .observeOn(MainScheduler.instance)
          .subscribeError { [unowned self] (_) in
            self.updateDeviceToken()
          }
          .addDisposableTo(disposeBag)
      }
  }

  func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                   fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
    if let aps = userInfo["aps"] as? NSDictionary {
      debugPrint(aps)
    }
  }
}
