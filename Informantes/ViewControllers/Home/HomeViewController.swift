//
//  HomeViewController.swift
//  Informantes-ios
//
//  Created on 5/16/17.
//  Copyright © 2017 Kogi. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

  // MARK: - IBOutlets

  // MARK: - Private Properties

  // MARK: - Internal Properties
  var viewModel: HomeViewModel

  // MARK: - Life cycle

  init(viewModel: HomeViewModel) {
    self.viewModel = viewModel
    super.init(nibName:String(describing: HomeViewController.self), bundle: nil)
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  override func viewDidLoad() {
    super.viewDidLoad()

    self.title = "Home".localized()

    // Do any additional setup after loading the view.
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }

}
