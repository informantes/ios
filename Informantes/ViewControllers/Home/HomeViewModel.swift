//
//  HomeViewModel.swift
//  Informantes-ios
//
//  Created on 5/16/17.
//  Copyright © 2017 Kogi. All rights reserved.
//

import Foundation

protocol HomeViewModelViewDelegate: class { }

protocol HomeViewModelCoordinatorDelegate : class {
  func homeViewModelShowMenu(_ viewModel: HomeViewModel)
}

class HomeViewModel {

  // MARK: - Internal properties
  weak var viewDelegate: HomeViewModelViewDelegate?
  weak var coordinatorDelegate: HomeViewModelCoordinatorDelegate?

  // MARK: - Initialization functions
  init() {

  }

  func showMenu() {
    coordinatorDelegate?.homeViewModelShowMenu(self)
  }

}
