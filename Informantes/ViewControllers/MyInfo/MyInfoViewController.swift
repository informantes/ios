//
//  MyInfoViewController.swift
//  Informantes-ios
//
//  Created on 6/29/17.
//  Copyright © 2017 Kogi. All rights reserved.
//

import UIKit
import SZTextView
import RxSwift

class MyInfoViewController: UIViewController {

  // MARK: - IBOutlets
  @IBOutlet weak var firstNameTextField: UITextField!
  @IBOutlet weak var lastNameTextField: UITextField!
  @IBOutlet weak var emailTextField: UITextField!
  @IBOutlet weak var cellTextField: UITextField!
  @IBOutlet weak var confirmButton: UIButton!

  // MARK: - Private Properties

  // MARK: - Internal Properties
  var viewModel: MyInfoViewModel
  fileprivate let disposeBag = DisposeBag()

  // MARK: - Life cycle

  init(viewModel: MyInfoViewModel) {
    self.viewModel = viewModel
    super.init(nibName:String(describing: MyInfoViewController.self), bundle: nil)
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")

  }

  override func viewDidLoad() {
    super.viewDidLoad()

    self.title = "My Info".localized()

    setupUI()
    setupRx()
    getCurrentLoggedIn()

    // Do any additional setup after loading the view.
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }

  // MARK: - Helpers
  private func setupUI() {
    customizeBackButton()

    confirmButton.customizeDefault()
  }

  private func setupRx() {

    firstNameTextField.rx.text
      .map { ($0?.trimmingCharacters(in: .whitespacesAndNewlines)) ?? "" }
      .bind(to: viewModel.firstname)
      .addDisposableTo(disposeBag)

    lastNameTextField.rx.text
      .map { ($0?.trimmingCharacters(in: .whitespacesAndNewlines)) ?? "" }
      .bind(to: viewModel.lastname)
      .addDisposableTo(disposeBag)

    emailTextField.rx.text
      .map { ($0?.trimmingCharacters(in: .whitespacesAndNewlines)) ?? "" }
      .bind(to: viewModel.email)
      .addDisposableTo(disposeBag)

    cellTextField.rx.text
      .map { ($0?.trimmingCharacters(in: .whitespacesAndNewlines)) ?? "" }
      .bind(to: viewModel.cellNumber)
      .addDisposableTo(disposeBag)

    viewModel.user.asObservable()
      .map { $0?.firstname }
      .bind(to: firstNameTextField.rx.text)
      .addDisposableTo(disposeBag)

    viewModel.user.asObservable()
      .map { $0?.lastname }
      .bind(to: lastNameTextField.rx.text)
      .addDisposableTo(disposeBag)

    viewModel.user.asObservable()
      .map { $0?.email }
      .bind(to: emailTextField.rx.text)
      .addDisposableTo(disposeBag)

    viewModel.user.asObservable()
      .map { $0?.cellNumber }
      .bind(to: cellTextField.rx.text)
      .addDisposableTo(disposeBag)
  }

  func getCurrentLoggedIn() {

    self.view.endEditing(true)
    LoaderView.show()
    viewModel.getMyProfile().observeOn(MainScheduler.instance)
      .doOnError { [unowned self] (error) in
        LoaderView.hide()
        UIAlertController.showFromViewController(self, forError: error)
      }
      .subscribeNext { [weak self] user in
        guard let strongSelf = self else { return }
        LoaderView.hide()
        strongSelf.viewModel.user.value = user
        strongSelf.viewModel.firstname.value = user.firstname ?? ""
        strongSelf.viewModel.lastname.value = user.lastname ?? ""
        strongSelf.viewModel.email.value = user.email ?? ""
        strongSelf.viewModel.cellNumber.value = user.cellNumber ?? ""
      }
      .addDisposableTo(disposeBag)
  }

  @IBAction func confirmButtonTapped(_ sender: UIButton) {
    self.view.endEditing(true)
    LoaderView.show()
    viewModel.updateUserInfo().observeOn(MainScheduler.instance)
      .doOnError { [unowned self] (error) in
        LoaderView.hide()
        UIAlertController.showFromViewController(self, forError: error)
      }
      .subscribeNext { _ in
        LoaderView.hide()

        UIAlertController.showFromViewController(self, title: "User update successful!".localized(), message: "", firstButtonTitle: "OK".localized(), firstButtonStyle: .default, firstButtonHandler: { [unowned self] in
          self.viewModel.updateSuccessful()
          }, secondButtonTitle: nil, secondButtonStyle: .default, secondButtonHandler: nil)
      }
      .addDisposableTo(disposeBag)
  }
}
