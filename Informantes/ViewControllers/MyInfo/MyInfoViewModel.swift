//
//  MyInfoViewModel.swift
//  Informantes-ios
//
//  Created on 6/29/17.
//  Copyright © 2017 Kogi. All rights reserved.
//

import Foundation
import RxSwift

protocol MyInfoViewModelViewDelegate: class {
  //func mapsDidChange(viewModel: MyInfoViewModel)
}

protocol MyInfoViewModelCoordinatorDelegate : class {
  func myInfoViewModelSuccessUpdate(_ viewModel: MyInfoViewModel)
}

class MyInfoViewModel {

  enum ValidationError: Error, CustomStringConvertible {
    case missingFirstname
    case missingLastname
    case missingEmail
    case missingCell
    var description: String {
      switch self {
      case .missingFirstname:
        return "You must enter a firstname".localized()
      case .missingLastname:
        return "You must enter a lastname".localized()
      case .missingEmail:
        return "You must enter a email".localized()
      case .missingCell:
        return "You must enter a cell number".localized()
      }
    }
  }

  // MARK: - Private properties
  fileprivate let disposeBag = DisposeBag()

  // MARK: - Internal properties
  weak var viewDelegate: MyInfoViewModelViewDelegate?
  weak var coordinatorDelegate: MyInfoViewModelCoordinatorDelegate?
  var user = Variable<User?>(nil)
  let firstname = Variable<String>("")
  let lastname = Variable<String>("")
  let email = Variable<String>("")
  let cellNumber = Variable<String>("")

  // MARK: - Initialization functions
  init() {
    setupRx()
  }

  // MARK: - Rx functions
  fileprivate func setupRx() {
    firstname.asObservable()
      .subscribeNext { self.user.value?.firstname = $0 }
    .addDisposableTo(disposeBag)

    lastname.asObservable()
      .subscribeNext { self.user.value?.lastname = $0 }
      .addDisposableTo(disposeBag)

    email.asObservable()
      .subscribeNext { self.user.value?.email = $0 }
      .addDisposableTo(disposeBag)

    cellNumber.asObservable()
      .subscribeNext { self.user.value?.cellNumber = $0 }
      .addDisposableTo(disposeBag)
  }

  // MARK: Internal Helpers
  func validate() throws {
    if firstname.value.characters.count == 0 {
      throw ValidationError.missingFirstname
    } else if lastname.value.characters.count == 0 {
      throw ValidationError.missingLastname
    } else if email.value.characters.count == 0 {
      throw ValidationError.missingEmail
    } else if cellNumber.value.characters.count == 0 {
      throw ValidationError.missingCell
    }
  }

  func updateSuccessful() {
    self.coordinatorDelegate?.myInfoViewModelSuccessUpdate(self)
  }

}
