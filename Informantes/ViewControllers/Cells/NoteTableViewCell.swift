//
//  NoteTableViewCell.swift
//  Informantes-ios
//
//  Created on 6/27/17.
//  Copyright © 2017 Kogi. All rights reserved.
//

import UIKit

class NoteTableViewCell: UITableViewCell {

  @IBOutlet weak var noteLabel: UILabel!

  override func awakeFromNib() {
    super.awakeFromNib()
    // Initialization code
  }

  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)

    // Configure the view for the selected state
  }

  func configureCell(text: String) {
    noteLabel.text = text
    noteLabel.customizeDefault()
  }

}
