//
//  Device.swift
//  Informantes-ios
//
//  Created on 6/2/17.
//  Copyright © 2017 Kogi. All rights reserved.
//

import Foundation
import Gloss

struct Device {

  static let deviceTokenKey = "deviceToken"
  var token: String = ""

}

extension Device: Decodable, Encodable {
  fileprivate struct JsonKeys {
    static let token = "token"
  }

  init?(json: JSON) {
    self.token = (JsonKeys.token <~~ json)!
  }

  func toJSON() -> JSON? {
    return jsonify([JsonKeys.token ~~> token])
  }
}
