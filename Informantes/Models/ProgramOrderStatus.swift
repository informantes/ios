//
//  ProgramOrderStatus.swift
//  cca-convention-swift
//
//  Created by Alex Arboleda on 6/21/17.
//  Copyright © 2017 Kogi. All rights reserved.
//

import Foundation
import Gloss

enum ExeptionReason: String {
  case closingLocation = "Closing Location"
  case doesNotHaveDisplay = "Does not have display"
  case dontUseProduct = "Don't use product"
  case financialDistress = "Financial distress"
  case issueWithVendor = "Issue with vendor"
  case noRoom = "No room"
  case regionDoesNotSupportProduct = "Region does not support product"
  case removedFromFloor = "Removed from floor"
  case takingAtOtherLocation = "Taking at other location"
  case other = "Other"
  case none = ""

  var value: String {
    return self.rawValue
  }
}

struct Program {

  var id: String?
  var statusId: String?
  var location: String?
  var quantity: Int?
  var exemptionStatus: String?
  var exemptionReason: ExeptionReason?
  var creditStatus: String?
  var creditStatusNote: String?
  var note: String?
  var sortOrder: Int?
  var type: String?
  var division: String?
  var vendor: String?
  var programNumber: String?
  var name: String?
  var description: String?
  var price: Float?
  var rebate: Float?
  var net: Float?
  var freight: FreightStatus?
  var vendorCode: String?
  var vendorCouponCode: String?
  var sendToHeadquarters: Bool?
  var imageUrl: URL?
}

extension Program {

  enum FreightStatus: String {
    case delivered = "Delivered"
    case plusFreight = "Plus Freight"
    case fboDistributor = "FBO Distributor"
    case interestOnly = "Interest Only"
    case none = ""

    var value: String {
      return self.rawValue
    }
  }

  enum OrderStatus: String {
    case exemptionApproved = "Exemption approved"
    case exemptionRequested = "Exemption requested"
    case ordered = "Ordered"
    case notOrdered = "Not ordered"

    var value: String {
      return self.rawValue
    }

    var color: UIColor {
      switch self {
      case .ordered:
        return UIColor.appFontGreenColor()
      default:
        return UIColor.appFontRedColor()
      }
    }
  }

  var status: OrderStatus {
    if self.exemptionStatus != nil {
      return exemptionStatus == "approved" ? OrderStatus.exemptionApproved : OrderStatus.exemptionRequested
    } else {
      return self.quantity! > 0 ? OrderStatus.ordered : OrderStatus.notOrdered
    }
  }

  var allExemptionReasons: [ExeptionReason] {
    let items: [ExeptionReason] = [.closingLocation, .doesNotHaveDisplay, .dontUseProduct, .financialDistress, .issueWithVendor, .noRoom, .regionDoesNotSupportProduct, .removedFromFloor, .takingAtOtherLocation, .other]
    return items
  }

  mutating func decrement() {
    guard let quantity = self.quantity, quantity > 0 else { return }
    self.quantity = quantity - 1
  }

  mutating func increment() {
    guard let quantity = self.quantity else { return }
    self.quantity = quantity + 1
  }
}

extension Program: Decodable, Encodable {

  static let pos = "programOrderStatus."
  static let program = "program."

  fileprivate struct JsonKeys {
    static let statusId = pos + "id"
    static let location = pos + "location"
    static let quantity = pos + "quantity"
    static let exemptionStatus = pos + "pexemptionStatus"
    static let exemptionReason = pos + "exemptionReason"
    static let creditStatus = pos + "creditStatus"
    static let creditStatusNote = pos + "creditStatusNote"
    static let note = pos + "note"
    static let id = program + "id"
    static let sortOrder = program + "sortOrder"
    static let type = program + "type"
    static let division = program + "division"
    static let vendor = program + "vendor"
    static let programNumber = program + "programNumber"
    static let name = program + "name"
    static let description = program + "description"
    static let price = program + "price"
    static let rebate = program + "rebate"
    static let net = program + "net"
    static let freight = program + "freight"
    static let vendorCode = program + "vendorCode"
    static let vendorCouponCode = program + "vendorCouponCode"
    static let sendToHeadquarters = program + "sendToHeadquarters"
    static let imageUrl = program + "imageUrl"
  }

  init?(json: JSON) {
    self.statusId = JsonKeys.statusId <~~ json
    self.location = JsonKeys.location <~~ json
    self.quantity = JsonKeys.quantity <~~ json
    self.exemptionStatus = JsonKeys.exemptionStatus <~~ json
    self.exemptionReason = JsonKeys.exemptionReason <~~ json
    self.creditStatus = JsonKeys.creditStatus <~~ json
    self.creditStatusNote = JsonKeys.creditStatusNote <~~ json
    self.note = JsonKeys.note <~~ json
    self.id = JsonKeys.id <~~ json
    self.sortOrder = JsonKeys.sortOrder <~~ json
    self.type = JsonKeys.type <~~ json
    self.division = JsonKeys.division <~~ json
    self.vendor = JsonKeys.vendor <~~ json
    self.programNumber = JsonKeys.programNumber <~~ json
    self.name = JsonKeys.name <~~ json
    self.description = JsonKeys.description <~~ json
    self.price = JsonKeys.price <~~ json
    self.rebate = JsonKeys.rebate <~~ json
    self.net = JsonKeys.net <~~ json
    self.freight = JsonKeys.freight <~~ json
    self.vendorCode = JsonKeys.vendorCode <~~ json
    self.vendorCouponCode = JsonKeys.vendorCouponCode <~~ json
    self.sendToHeadquarters = JsonKeys.sendToHeadquarters <~~ json
    self.imageUrl = JsonKeys.imageUrl <~~ json
  }

  func toJSON() -> JSON? {
    return jsonify([JsonKeys.statusId ~~> statusId,
                    JsonKeys.location ~~> location,
                    JsonKeys.quantity ~~> quantity,
                    JsonKeys.exemptionStatus ~~> exemptionStatus,
                    JsonKeys.exemptionReason ~~> exemptionReason,
                    JsonKeys.creditStatus ~~> creditStatus,
                    JsonKeys.creditStatusNote ~~> creditStatusNote,
                    JsonKeys.note ~~> note,
                    JsonKeys.id ~~> id,
                    JsonKeys.sortOrder ~~> sortOrder,
                    JsonKeys.type ~~> type,
                    JsonKeys.division ~~> division,
                    JsonKeys.vendor ~~> vendor,
                    JsonKeys.programNumber ~~> programNumber,
                    JsonKeys.name ~~> name,
                    JsonKeys.description ~~> description,
                    JsonKeys.price ~~> price,
                    JsonKeys.rebate ~~> rebate,
                    JsonKeys.net ~~> net,
                    JsonKeys.freight ~~> freight,
                    JsonKeys.vendorCode ~~> vendorCode,
                    JsonKeys.vendorCouponCode ~~> vendorCouponCode,
                    JsonKeys.sendToHeadquarters ~~> sendToHeadquarters,
                    JsonKeys.imageUrl ~~> imageUrl])
  }
}
